things to review:
* bad variable names
* not snake case variable names
* class naming (PascalCase)
* range loop instead of iterator
* not all branches return function
* format string that doesn't format for arg
* big function
* catching Exception
* using semicolons in python
* cyclic import
* function with a lot of arguments
* lots of nested conditionals
* no tests
* big tuple return
* spelling errors
* bad indexing (index max len > len by 1)
* import module that does nothing
* print statements in code
* unhashable type
* global variables
