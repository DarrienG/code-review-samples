#!/usr/bin/env python3

import argparse


def main():
    parser = argparse.ArgumentParser(description="ultimate printer")
    parser.add_argument("to_print", help="item to print 10 times")
    args = parser.parse_args()
    print_arg(args.to_print)


def print_arg(arg: str):
    for _ in range(0, 10):
        print(arg)


if __name__ == "__main__":
    main()
