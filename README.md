# The printer

Print anything 10 times

My favorite script :)

Run with:

```bash
./main.py yeehaw
```

And it will print:

```
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
yeehaw
```

Isn't that cool? :D
